---
sidebarDepth: 2
---

# Tech Stack

Listing of applications and services we use at Meltano.

## [1Password](https://meltano.1password.com/)

Password manager.

Every team member has their own account.

## [Algolia](https://www.algolia.com/)

Search on <https://meltano.com>.

Account details are in the `Engineering` 1Password vault.

## [Amazon Web Services](https://aws.amazon.com/)

Domain name registration of `meltano.com`, `meltano.io`, `meltano.net`, and `meltano.org`.

Account details are in the `Engineering` 1Password vault.

## [Carta](https://carta.com/)

Cap table and stock options management.

Every team member has their own account.
Main account details are in the `Finance` 1Password vault.

## [DigitalOcean](https://www.digitalocean.com/)

Hosting and DNS management for `*.meltanodata.com`.

Account details are in the `Engineering` 1Password vault.

## [DockerHub](https://hub.docker.com/u/meltano)

Hosting of [`meltano/meltano`](https://hub.docker.com/r/meltano/meltano) Docker image.

Account details are in the `Engineering` 1Password vault.

## [Expensify](https://www.expensify.com/)

Expense tracking.

Every team member has their own account.
Main account details are in the `Finance` 1Password vault.

## [Gandi](https://admin.gandi.net/)

Domain name registration of `meltano.app`.

Account details are in the `Engineering` 1Password vault.

## [GitLab](https://gitlab.com/meltano/)

DevOps platform.

Every team member has their own account.

## [Google Ads](https://ads.google.com/)

Advertisement platform.

Select team members have access through their [Google Workspace](#google-workspace) account.

## [Google Analytics](https://analytics.google.com/)

Analytics for <https://meltano.com>, <https://hub.meltano.com>, <https://sdk.meltano.com>, [Substack](#substack), CLI, and UI.

Select team members have access through their [Google Workspace](#google-workspace) account.

## [Google Search Console](https://search.google.com/search-console)

Search performance for <https://meltano.com>.

Select team members have access through their [Google Workspace](#google-workspace) account.

## [Google Voice](https://voice.google.com/)

US phone number for non-US-based team members.

Connected to [Google Workspace](#google-workspace).

## [Google Workspace](https://workspace.google.com/)

`<name>@meltano.com` email addresses and related Google services:

- [Gmail](https://gmail.com/)
- [Google Calendar](https://calendar.google.com/)
- [Google Drive](https://drive.google.com/)
  - [Google Docs](https://docs.google.com/document/)
  - [Google Sheets](https://docs.google.com/spreadsheets/)
  - [Google Slides](https://docs.google.com/presentation/)

Every team member has their own account.
Main account details are in the `Executive` 1Password vault.

## [Greenhouse](https://www.greenhouse.io/)

Manage job applicants, our job board, and our hiring process.

Every team member has their own account.

## [Instagram](https://www.instagram.com/)

[`meltanodata`](https://www.instagram.com/meltanodata/) Instagram account.

Account details are in the `Social` 1Password vault.

## [Intercom](https://www.intercom.com/)

Chat button on bottom right of <https://meltano.com/> and Meltano UI.

Select team members have their own account.

## [LinkedIn](https://linkedin.com/company/meltano/)

Meltano company page.

Select team members have access through their personal LinkedIn account.

## [Mercury](https://mercury.com/)

Bank account and credit cards.

Select team members have their own account.
Main account and credit card details are in the `Finance` 1Password vault.

## [NameCheap](https://www.namecheap.com/)

Domain name registration for `meltanodata.com` and `meltano.dev`.
Domain name registration and DNS management `singerhub.io`.

Account details are in the `Engineering` 1Password vault.

## [Netlify](https://www.netlify.com/)

Feature branch previews for <https://gitlab.com/meltano/meltano> docs.

Account details are in the `Engineering` 1Password vault.

## [Pingdom](https://my.pingdom.com/)

Downtime alerts for <https://meltano.com> and <https://meltano.meltanodata.com>.

Account details are in the `Engineering` 1Password vault.

## [PTO by Roots](https://meltano.slack.com/apps/AELEX1TU3-pto-by-roots)

[PTO by Roots](https://www.tryroots.io/pto) allows employees and managers to coordinate time off seamlessly with intuitive Slack commands. PTO by Roots also reminds employees of their upcoming time off and helps them assign roles and tasks for co-workers, giving all parties involved greater alignment and peace of mind. 

Every team member has access through their [Slack](#slack) account (under Apps).

## [PyPI](https://pypi.org/)

Python package index for [`meltano`](https://pypi.org/project/meltano/) and [`singer-sdk`](https://pypi.org/project/singer-sdk/) packages.

Select team members have access through their own account.
Main account details are in the `Engineering` 1Password vault.

## [QuickBooks Online](https://app.qbo.intuit.com/)

Bookkeeping.

Account details are in the `Finance` 1Password vault.

## [Read the Docs](https://readthedocs.org/)

Docs hosting for <https://gitlab.com/meltano/sdk>: <https://sdk.meltano.com>.

Account details are in the `Engineering` 1Password vault.

## [Remote](https://remote.com/)

International payroll, PEO, and contractors.

Non-US team members have their own account.
Main account details are in the `HR` 1Password vault.

## [SendGrid](https://app.sendgrid.com/)

Currently unused.

Account details are in the `Engineering` 1Password vault.

## [SiteGround](https://ua.siteground.com/)

Hosting for <https://meltano.com>.
DNS management for `meltano.com`, `meltano.org`, `meltano.io`, `meltano.net`, `meltano.app`, and `meltano.dev`.

Account details are in the `Engineering` 1Password vault.

## [Slack](https://meltano.slack.com/)

Chat with the community and the team.

Every team member has their own account.

## [Snowflake](https://mca68843.snowflakecomputing.com/)

Data warehouse.

Select team members have their own account.

## [Substack](https://meltano.substack.com/)

Newsletters.

Account details are in the `Social` 1Password vault.

## [TikTok](https://www.tiktok.com/@meltanodata)

[`@meltanodata`](https://www.tiktok.com/@meltanodata) TikTok account.

Account details are in the `Social` 1Password vault.

## [Twitter](https://twitter.com/meltanodata)

[`@MeltanoData`](https://twitter.com/meltanodata) Twitter account.

Account details are in the `Social` 1Password vault.

## [WordPress](https://meltano.com/blog/wp-admin/)

Blog at <https://meltano.com/blog/>.

Every team member has their own account.
Main account details are in the `Marketing` 1Password vault.

## [YouTube](https://www.youtube.com/meltano)

[`meltano`](https://www.youtube.com/meltano) YouTube channel.

Every team member has access through their [Google Workspace](#google-workspace) account.

## [Zapier](https://zapier.com/)

Custom integrations between GitLab, Slack, and others.

Account details are in the `Engineering` 1Password vault.

## [Zendesk](https://meltano.zendesk.com/)

Receives email sent to `hello@meltano.com` and `security@meltano.com`.

Select team members have their own account.

## [Zoom](https://zoom.us/)

Video calls.

Every team member has their own account.
