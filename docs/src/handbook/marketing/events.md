---
title: "Events"
sidebarDepth: 2
---

# Events

When considering or suggesting an event please use the issue template below as your starting point.
- [Issue template](https://gitlab.com/meltano/marketing/marketing-general/-/blob/main/.gitlab/issue_templates/event.md)
- [Event calendar](https://calendar.google.com/calendar/embed?src=c_leig73ktuh9hhbic22h8a39mb4%40group.calendar.google.com&ctz=America%2FNew_York)

## Criteria for evaluating and event
* audience 
* location
* attendance numbers
* speaking opportunities
* our goals
* timing
* cost and resources needed to get the most our of our investment

Each one of these criteria is given a score on how much it can move the needle in each of the areas listed above and we make a decision on wheather or not to participate form there. 

We will not participate if we do not have enough time or resources to do something well and to make the most of the opportunity no matter how much if a fit is in all the other areas. 

## Event Campaigns
We do not run events as stand alone activities they need to fit into out larger strategy plans and need to involve robust marketing campaigns surrounding them. 




