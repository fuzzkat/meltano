---
title: "Meltano Brand Kit"
sidebarDepth: 2
---

## Meltano Brand Kit

### Brand Personality
Our personality is built around four main characteristics.

* **Human**: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and honestly.
* **Competent**: We aim to be masterly at what we offer, and communicate with conviction.
* **Humble**: We care about helping those around us achieve great things more than we care about our personal accomplishments.
* **Inclusive**: We embrace new ideas, new voices and new perspectives. We are always happy to be educated.
* **Fun**: We like Shramp and other oddities. 

### Tone of Voice
Friendly, informative yet ambitious. We bring empathy to all interactions and stewardship to those new to the space. 

### Brand Resources
Coming Soon! (Fonts, Colors, Logos, Slide decks and much more)


### Talking about the Meltano Product Family and Team

As of 2021 we have 3 main "product lines". Each of these should be referenced in speech and text communications in specific ways.

### Meltano

#### Product/Project
  
**Recommended**

    * Meltano
    * The Meltano Open-Source Project

     Example usage: Last week, a new version for Meltano was released

**Incorrect**

    * The Meltano

    Example usage: Last week, a new version for the Meltano was released

### SDK

Note that Targets can be substituted for Taps in each of these examples

**Recommended**

    * The Meltano SDK
    * The Meltano Tap SDK
    * The Meltano SDK for Taps
    * The Meltano SDK for Singer Taps

    Example usage: Today we're releasing a new update for the Meltano Tap SDK that...

**OK**

    * The SDK
    * The Tap SDK
    * The SDK for Taps
    * The SDK for Singer Taps
    * Meltano's Tap SDK
    * The Tap SDK by Meltano

    These are OK when the proper, Meltano-specific name has already been referenced. 
    Example usage: Today we're releasing a new update for the SDK for Taps that...

**Incorrect**

    * Singer SDK
    * Singer SDK for Taps
    * singer-sdk
    * Singer's SDK
    * Meltano's Singer SDK
    * Singer SDK by Meltano

    Incorrect example usage: Today we're releasing a new update for the Singer SDK for Taps that...

### MeltanoHub

**Recommended**

    * MeltanoHub
    * MeltanoHub for Singer
    * The Hub

    Example usage: Today we're releasing an update for MeltanoHub for Singer to...

**Incorrect**

    * The MeltanoHub
    * The SingerHub
    * The Hub for Singer

    Incorrect example usage: Today we're releasing an update for the MeltanoHub for Singer to...

#### Team

**Recommended**

    * the Meltano Team
    * the Meltano Core Team

    Example usage: The Meltano Team is announcing today...

**Incorrect**

    * The Meltano

    Incorrect example usage: The Meltano is announcing today...
